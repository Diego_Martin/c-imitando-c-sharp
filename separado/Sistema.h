#ifndef __SISTEMA_H__
#define __SISTEMA_H__

#include <ctype.h>
#include <iostream>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <typeinfo>

#define __FUNC_INFO__ __PRETTY_FUNCTION__

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
	//define something for Windows (32-bit and 64-bit, this part is common)
	#define SO "Windows"
	#define MiWindows
	#include <windows.h>
	#ifdef _WIN64
		//define something for Windows (64-bit only)
		#define ARQUITECTURA "x64"
	#else
		//define something for Windows (32-bit only)
		#define ARQUITECTURA "x32"
	#endif
#elif __APPLE__
	#include <TargetConditionals.h>
	#if TARGET_IPHONE_SIMULATOR
		// iOS Simulator
		#define SO "Simulador IOS"
		#define ARQUITECTURA ""
	#elif TARGET_OS_IPHONE
		// iOS device
		#define SO "IOS"
		#define ARQUITECTURA ""
	#elif TARGET_OS_MAC
		// Other kinds of Mac OS
		#define SO "Mac OS"
		#define ARQUITECTURA ""
	#else
		#define SO "Apple desconocido"
		#define ARQUITECTURA ""
	#endif
#elif __linux__
	#define MiLinux
	#include <unistd.h>
	#define SO "Linux"
	#define ARQUITECTURA ""
#elif __unix__ // all unices not caught above
	#define MiUnix
	#include <unistd.h>
	#define SO "Unix"
	#define ARQUITECTURA ""
#elif defined(_POSIX_VERSION)
	#define MiPosix
	#include <unistd.h>
	#define SO "Posix"
	#define ARQUITECTURA ""
#else
	#define SO "Compilador desconocido"
	#define ARQUITECTURA ""
#endif


typedef long long int llint;

class CColor
{
	public:
		static const int NegroOscuro = 30;
		static const int RojoOscuro = 31;
		static const int VerdeOscuro = 32;
		static const int AmarilloOscuro = 33;
		static const int AzulOscuro = 34;
		static const int MagentaOscuro = 35;
		static const int CyanOscuro = 36;
		static const int BlancoOscuro = 37;
		static const int NegroClaro = 90;
		static const int RojoClaro = 91;
		static const int VerdeClaro = 92;
		static const int AmarilloClaro = 93;
		static const int AzulClaro = 94;
		static const int MagentaClaro = 95;
		static const int CyanClaro = 96;
		static const int BlancoClaro = 97;

		int longitud () {
			return 16;
		}

		int *listar () {
			int *lista = (int *) malloc (sizeof (int) * 16);
			int valores[16] = {NegroOscuro, RojoOscuro, VerdeOscuro, AmarilloOscuro, AzulOscuro, MagentaOscuro, CyanOscuro, BlancoOscuro, NegroClaro, RojoClaro, VerdeClaro, AmarilloClaro, AzulClaro, MagentaClaro, CyanClaro, BlancoClaro};
			for (int i = 0; i < 16; i++)
				*(lista + i) = valores[i];
			return lista;
		}
};

class FFondo
{
	public:
		static const int NegroOscuro = 40;
		static const int RojoOscuro = 41;
		static const int VerdeOscuro = 42;
		static const int AmarilloOscuro = 43;
		static const int AzulOscuro = 44;
		static const int MagentaOscuro = 45;
		static const int CyanOscuro = 46;
		static const int BlancoOscuro = 47;
		static const int NegroClaro = 100;
		static const int RojoClaro = 101;
		static const int VerdeClaro = 102;
		static const int AmarilloClaro = 103;
		static const int AzulClaro = 104;
		static const int MagentaClaro = 105;
		static const int CyanClaro = 106;
		static const int BlancoClaro = 107;

		int longitud () {
			return 16;
		}

		int *listar () {
			int *lista = (int *) malloc (sizeof (int) * 16);
			int valores[16] = {NegroOscuro, RojoOscuro, VerdeOscuro, AmarilloOscuro, AzulOscuro, MagentaOscuro, CyanOscuro, BlancoOscuro, NegroClaro, RojoClaro, VerdeClaro, AmarilloClaro, AzulClaro, MagentaClaro, CyanClaro, BlancoClaro};
			for (int i = 0; i < 16; i++)
				*(lista + i) = valores[i];
			return lista;
		}
};

class CConsola
{
	private:
		int limpiar;
		int currentTexto;
		int currentFondo;
	public:
		CColor Colores;
		FFondo Fondos;
		CConsola() {
			currentTexto = Colores.BlancoClaro;
			currentFondo = Fondos.NegroOscuro;
			this->SetColorTexto(this->Colores.BlancoClaro);
			this->SetColorFondo(this->Fondos.NegroOscuro);
		}

		int Escribir (const char *formato = "", ...)
		{
			va_list argumentos;
			int exito = 1;

			va_start (argumentos, formato);
			exito = vfprintf (stdout, formato, argumentos);
			va_end (argumentos);

			if (limpiar)
				this->Limpiar(stdout);

			return exito;
		}

		int EscribirLinea (const char *formato = "", ...)
		{
			va_list argumentos;
			int exito = 1;
			char aux[0x1000];

			strcpy (aux, formato);
			strcat (aux, "\n");
			formato = (const char *) aux;

			va_start (argumentos, formato);
			exito = vfprintf (stdout, formato, argumentos);
			va_end (argumentos);

			if (limpiar)
				this->Limpiar(stdout);

			return exito;
		}

		void EsperaTecla (const char *pretexto = "") {
			this->Escribir (pretexto);
			getchar();
		}

		int SetColorTexto (int color) {
			int exito = printf ("\033[%i;%im", color, currentFondo);
			currentTexto = color;
			if (exito)
				return 1;
			this->Fallo ("Algo ha ido mal al asignar el color de texto.");
			exit (EXIT_FAILURE);
		}

		int SetColorFondo (int color) {
			int exito = printf ("\033[%i;%im", currentTexto, color);
			currentFondo = color;
			if (exito)
				return 1;
			this->Fallo ("Algo ha ido mal al asignar el color de texto.");
			exit (EXIT_FAILURE);
		}

		int SetColorConsola (int colorTexto, int colorFondo) {
			int exito = printf ("\033[%i;%im", colorTexto, colorFondo);
			currentFondo = colorFondo;
			currentTexto = colorTexto;
			if (exito)
				return 1;
			this->Fallo ("Algo ha ido mal al asignar el color de texto.");
			exit (EXIT_FAILURE);
		}

		void Fallo (const char *texto) {
			printf ("\033[%i;40m", this->Colores.RojoClaro);
			this->EscribirLinea ("%s", texto);
			printf ("\033[%i;40m", this->Colores.BlancoClaro);
		}

		void Aviso (const char *texto) {
			printf ("\033[%i;40m", this->Colores.AmarilloClaro);
			this->EscribirLinea ("%s", texto);
			printf ("\033[%i;40m", this->Colores.BlancoClaro);
		}

		void Exito (const char *texto) {
			printf ("\033[%i;40m", this->Colores.VerdeClaro);
			this->EscribirLinea ("%s", texto);
			printf ("\033[%i;40m", this->Colores.BlancoClaro);
		}

		int Limpiar (FILE * canal) {
			int exito = fflush (canal);
			if (!exito)
				return 1;
			else {
				this->Fallo ("Ha habido algun error al limpiar el archivo indicado.");
				exit (EXIT_FAILURE);
			}
		}

		void SetLimpiar (bool modo = true) {
			limpiar = modo;
		}

		bool GetLimpiar () {
			return limpiar;
		}
};

class AAleatorio
{
	private:
	public:
		AAleatorio() {
			srand (time(NULL));
		};
		// ~AAleatorio();
		int Ent (int min = 0, int max = 0) {
			if (min == 0 && max == 0) 
				return rand();
			
			if (min > 0 && max == 0) 
				return rand() % (min + 1);
			
			if (min < 0 && max == 0) 
				return -1 * (rand() % (abs(min) + 1));
			
			if (min < 0 && max > 0)
				return rand() % ((max + 1) - min) + min;

			if (min < max)
				return rand() % ((max + 1) - min) + min;
			else {
				fprintf (stderr, "El mínimo debe ser menor que el máximo.\n");
				exit (EXIT_FAILURE);
			}
		}

		char Car () {
			return (char) ((rand() % 223) + 32);
		}

		char Letra (const char caso = 'a') {
			if (caso == 'M')
				return (char) ((rand() % ('Z' - 'A')) + 'A');
			else if (caso == 'm')
				return (char) ((rand() % ('z' - 'a')) + 'a');
			else
				return (rand() % 2) ? (char) ((rand() % ('z' - 'a')) + 'a') : (char) ((rand() % ('Z' - 'A')) + 'A');
			
		}
};

class RReloj
{
	private:
		time_t temporizador;
		struct tm *momento;
		int Hora;
		int Minuto;
		int Segundo;

	public:
		RReloj () {
			this->temporizador = time(NULL);
			this->momento = localtime (&this->temporizador);
			this->Hora = this->momento->tm_hour;
			this->Minuto = this->momento->tm_min;
			this->Segundo = this->momento->tm_sec;
		}

		RReloj (int hora, int min, int sec) {
			this->Hora = hora;
			this->Minuto = min;
			this->Segundo = sec;
		}

		RReloj GetHoraActualObj () {
			RReloj res;
			return res;
		}

		int GetHora () {
			momento = localtime (&temporizador);
			Hora = momento->tm_hour;
			return Hora;
		}

		int GetMinuto () {
			momento = localtime (&temporizador);
			Minuto = momento->tm_min;
			return Minuto;
		}

		int GetSegundos () {
			momento = localtime (&temporizador);
			Segundo = momento->tm_sec;
			return Segundo;
		}

		char *GetFechaActualStr (const char *formato = "H:m:s") {
			time_t temp_temporizador = time(NULL);
			struct tm *temp_momento = localtime (&temp_temporizador);
			int temp_Hora = temp_momento->tm_hour;
			int temp_Min = temp_momento->tm_min;
			int temp_Sec = temp_momento->tm_sec;
			char *res = (char *) malloc (sizeof (char) * strlen (formato));
			if (strcmp (formato, "H:m:s") == 0)
				sprintf (res, "%02i:%02i:%02i", Hora, Minuto, Segundo);
			else if (strcmp (formato, "s:m:H") == 0)
				sprintf (res, "%02i:%02i:%02i", Segundo, Minuto, Hora);
			else if (strcmp (formato, "H:m") == 0)
				sprintf (res, "%02i:%02i", Hora, Minuto);
			else if (strcmp (formato, "m:s") == 0)
				sprintf (res, "%02i:%02i", Minuto, Segundo);
			else {
				printf ("\033[31;40m");
				printf ("Formato de hora no valido.");
				printf ("\033[37;40m");
				exit (EXIT_FAILURE);
			}
			return res;
		}

		char *toString (const char *formato = "H:m:s") {
			char *res = (char *) malloc (sizeof (char) * strlen (formato));
			if (strcmp (formato, "H:m:s") == 0)
				sprintf (res, "%02i:%02i:%02i", Hora, Minuto, Segundo);
			else if (strcmp (formato, "s:m:H") == 0)
				sprintf (res, "%02i:%02i:%02i", Segundo, Minuto, Hora);
			else if (strcmp (formato, "H:m") == 0)
				sprintf (res, "%02i:%02i", Hora, Minuto);
			else if (strcmp (formato, "m:s") == 0)
				sprintf (res, "%02i:%02i", Minuto, Segundo);
			else {
				printf ("\033[31;40m");
				printf ("Formato de hora no valido.");
				printf ("\033[37;40m");
				exit (EXIT_FAILURE);
			}
			return res;
		}
		// ~ObjetoTiempo();
};

class CCalendario
{
	private:
		time_t temporizador;
		struct tm *momento;
		int Anio;
		int Mes;
		int Dia;

	public:
		CCalendario () {
			this->temporizador = time(NULL);
			this->momento = localtime (&this->temporizador);
			this->Anio = this->momento->tm_year;
			this->Mes = this->momento->tm_mon;
			this->Dia = this->momento->tm_mday;
		}

		CCalendario (int hora, int min, int sec) {
			this->Anio = hora;
			this->Mes = min;
			this->Dia = sec;
		}

		CCalendario GetFechaActualObj () {
			CCalendario res;
			return res;
		}

		int TodosLosDias () {
			return time (NULL) / (3600 * 24);
		}

		int TodosLosMeses () {
			return (int) time (NULL) / (3600 * 24 * 30);
		}

		int TodosLosAnios () {
			return (int) time (NULL) / (3600 * 24 * 365);
		}

		char *GetFechaActualStr (const char *formato = "dd-MM-YYYY") {
			time_t temp_temporizador = time(NULL);
			struct tm *temp_momento = localtime (&temp_temporizador);
			int temp_Anio = temp_momento->tm_year;
			int temp_Mes = temp_momento->tm_mon;
			int temp_Dia = temp_momento->tm_mday;
			char *res = (char *) malloc (sizeof (char) * strlen(formato));
			if (strcmp(formato, "dd-MM-YYYY") == 0)
				sprintf (res, "%02i-%02i-%04i", temp_Dia, (temp_Mes + 1), (temp_Anio + 1900));
			else if (strcmp(formato, "dd-MM-YY")== 0)
				sprintf (res, "%02i-%02i-%02i", temp_Dia, (temp_Mes + 1), (int) ((temp_Anio + 1900) / 100));
			else if (strcmp(formato, "YYYY-MM-dd") == 0)
				sprintf (res, "%04i-%02i-%02i", (temp_Anio + 1900), (temp_Mes + 1), temp_Dia);
			else if (strcmp(formato, "YY-MM-dd") == 0)
				sprintf (res, "%02i-%02i-%02i", (int) ((temp_Anio + 1900) / 100), (temp_Mes + 1), temp_Dia);
			else if (strcmp(formato, "dd-MM") == 0)
				sprintf (res, "%02i-%02i", temp_Dia, (temp_Mes + 1));
			else if (strcmp(formato, "MM-dd") == 0)
				sprintf (res, "%02i-%02i", (temp_Mes + 1), temp_Dia);
			else if (strcmp(formato, "dd/MM/YYYY") == 0)
				sprintf (res, "%02i/%02i/%04i", temp_Dia, (temp_Mes + 1), (temp_Anio + 1900));
			else if (strcmp(formato, "dd/MM/YY") == 0)
				sprintf (res, "%02i/%02i/%02i", temp_Dia, (temp_Mes + 1), (int) ((temp_Anio + 1900) / 100));
			else if (strcmp(formato, "YYYY/MM/dd") == 0)
				sprintf (res, "%04i/%02i/%02i", (temp_Anio + 1900), (temp_Mes + 1), temp_Dia);
			else if (strcmp(formato, "YY/MM/dd") == 0)
				sprintf (res, "%02i/%02i/%02i", (int) ((temp_Anio + 1900) / 100), (temp_Mes + 1), temp_Dia);
			else if (strcmp(formato, "dd/MM") == 0)
				sprintf (res, "%02i/%02i", temp_Dia, (temp_Mes + 1));
			else if (strcmp(formato, "MM/dd") == 0)
				sprintf (res, "%02i/%02i", (temp_Mes + 1), temp_Dia);
			else {
				printf ("\033[31;40m");
				printf ("Formato de fecha no valido.");
				printf ("\033[37;40m");
				exit (EXIT_FAILURE);
			}
			return res;
		}

		char *toString (const char *formato = "dd-MM-YYYY") {
			char *res = (char *) malloc (sizeof (char) * strlen(formato));
			if (strcmp(formato, "dd-MM-YYYY") == 0)
				sprintf (res, "%02i-%02i-%04i", Dia, (Mes + 1), (Anio + 1900));
			else if (strcmp(formato, "dd-MM-YY") == 0)
				sprintf (res, "%02i-%02i-%02i", Dia, (Mes + 1), (int) ((Anio + 1900) / 100));
			else if (strcmp(formato, "YYYY-MM-dd") == 0)
				sprintf (res, "%04i-%02i-%02i", (Anio + 1900), (Mes + 1), Dia);
			else if (strcmp(formato, "YY-MM-dd") == 0)
				sprintf (res, "%02i-%02i-%02i", (int) ((Anio + 1900) / 100), (Mes + 1), Dia);
			else if (strcmp(formato, "dd-MM") == 0)
				sprintf (res, "%02i-%02i", Dia, (Mes + 1));
			else if (strcmp(formato, "MM-dd") == 0)
				sprintf (res, "%02i-%02i", (Mes + 1), Dia);
			else if (strcmp(formato, "dd/MM/YYYY") == 0)
				sprintf (res, "%02i/%02i/%04i", Dia, (Mes + 1), (Anio + 1900));
			else if (strcmp(formato, "dd/MM/YY") == 0)
				sprintf (res, "%02i/%02i/%02i", Dia, (Mes + 1), (int) ((Anio + 1900) / 100));
			else if (strcmp(formato, "YYYY/MM/dd") == 0)
				sprintf (res, "%04i/%02i/%02i", (Anio + 1900), (Mes + 1), Dia);
			else if (strcmp(formato, "YY/MM/dd") == 0)
				sprintf (res, "%02i/%02i/%02i", (int) ((Anio + 1900) / 100), (Mes + 1), Dia);
			else if (strcmp(formato, "dd/MM") == 0)
				sprintf (res, "%02i/%02i", Dia, (Mes + 1));
			else if (strcmp(formato, "MM/dd") == 0)
				sprintf (res, "%02i/%02i", (Mes + 1), Dia);
			else {
				printf ("\033[31;40m");
				printf ("Formato de fecha no valido.");
				printf ("\033[37;40m");
				exit (EXIT_FAILURE);
			}
			return res;
		}
		// ~ObjetoTiempo();
};

class TTiempo
{
	private:
	public:
		RReloj Reloj;
		CCalendario Calendario;
		int TodosLosSegundos () {
			return time (NULL);
		}

		int TodosLosMinutos () {
			return time (NULL) / 60;
		}

		int TodasLasHoras () {
			return time (NULL) / 3600;
		}

		char *GetHoraInicioDeProgramaStr (const char *formato = "H:m:s") {
			return Reloj.toString(formato);
		}

		char *GetFechaInicioDeProgramaStr (const char *formato = "dd-MM-YYYY") {
			return Calendario.toString(formato);
		}

		RReloj GetHoraInicioDePrograma () {
			return Reloj;
		}

		CCalendario GetFechaInicioDePrograma () {
			return Calendario;
		}

		char *GetFechaActualStr (const char *formato = "dd-MM-YYYY") {
			return Calendario.GetFechaActualStr (formato);
		}

		void Esperar (double seg) {
			#ifdef MiWindows
				Sleep ((int) (seg * 1000));
			#elif defined MiLinux
				usleep ((int) (seg * 1000000));
			#endif
		}
		// TTiempo();
		// ~TTiempo();
};

class CConvertir
{
	private:
	public:
		// CConvertir();
		// ~CConvertir();
	
};

class MMatematicas
{
	private:
	public:
		template <class Type, class Ret>
		struct Retorno {
			Type tipo;
			Ret retorno;
		};
		template <class Type, class Ret>
		Ret Potencia (Type base, Type exp) {
			int signo = 1;
			if (!(typeid (Type) == typeid (int) || typeid (Type) == typeid (int))) {
				printf ("\033[31;40m");
				printf ("Tanto base como exponente deben ser numeros enteros.");
				printf ("\033[37;40m");
				exit (EXIT_FAILURE);
			}
			if (exp == 0)
				return (llint) 1;

			bool dividir = (exp < 0);
			if (base < 0)
				if (exp % 2)
					signo = -1;
			if (!(typeid (Ret) == typeid (double) && signo < 0)) {
				printf ("\033[31;40m");
				printf ("Si el exponente es negativo, el retorno debe ser \033[35;40mdouble\033[31;40m.");
				printf ("\033[37;40m");
				exit (EXIT_FAILURE);
			}

			if (!(typeid (Ret) == typeid (llint) && signo > 0)) {
				printf ("\033[31;40m");
				printf ("Si el exponente es positivo, el retorno debe ser \033[35;40mlong long int\033[31;40m.");
				printf ("\033[37;40m");
				exit (EXIT_FAILURE);
			}

			llint res = 1;
			for (int i = 0; i < exp; i++)
				res *= abs(base);

			if (!dividir)
				return (llint) (signo * res);

			return (double) (1 / (signo * res));
		}
		// MMatematicas();
		// ~MMatematicas();
	
};

class SSistema
{
	private:
	public:
		static const int SALIR_EXITO = 0;
		static const int SALIR_FALLO = 1;
		void Salir (int success, const char *texto = "") {
			printf("\033[%i;%im\n", this->Consola.Colores.BlancoClaro, this->Consola.Fondos.NegroOscuro);
			if (success == 0)
				exit (success);
			else {
				Consola.Fallo (texto);
				exit (success);
			}
		}

		void Ejecutar (const char *comando) {
			system (comando);
		}

		int Sonar () {
			if (printf("\a"))
				return 1;

			this->Salir (SALIR_FALLO, "No se ha podido reproducir el sonido.");
			return 0;
		}

		char *GetSO () {
			char *res = NULL;
			res = (char *) malloc (sizeof (char) * strlen (SO));
			sprintf (res, "%s", SO);
			return res;
		}

		char *GetArquitectura () {
			char *res = NULL;

			#ifdef MiWindows
				res = (char *) malloc (sizeof (char) * strlen (ARQUITECTURA));
				sprintf (res, "%s", ARQUITECTURA);
			#elif defined MiLinux
				res = (char *) malloc (sizeof (char) * strlen ("No se pudo obtener la informacion"));
				sprintf (res, "No se pudo obtener la informacion");
			#elif defined MiUnix
				res = (char *) malloc (sizeof (char) * strlen ("No se pudo obtener la informacion"));
				sprintf (res, "No se pudo obtener la informacion");
			#elif defined MiPosix
				res = (char *) malloc (sizeof (char) * strlen ("No se pudo obtener la informacion"));
				sprintf (res, "No se pudo obtener la informacion");
			#endif

			return res;
		}
		SSistema();
		~SSistema();
		CConsola Consola;
		CConvertir Convertir;
		AAleatorio Aleatorio;
		MMatematicas Matematicas;
		TTiempo Tiempo;
};

SSistema Sistema;

#endif